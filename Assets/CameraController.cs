﻿using UnityEngine;
using System.Collections;

/**Camera controller adjusts the camera based on user input
 * The mouse moves the camera vertically and horizontally
 * The keyboard rolls the camera and moves it forwards and backwards
 **Modified by Stewart Collins - 326206 - Last Edit 11/09/16
 */
public class CameraController : MonoBehaviour {
    //The speed of vertical rotation
    public float verticalRotationSpeed = .01f;
    //The soeed of horizontal rotation
    public float horizontalRotationSpeed = 0.1f;
    //The speed of roll
    public float rollSpeed = 10;
    //The speed of movement
    public float moveSpeed = 100;
    //Stores the last position of the mouse
    private Vector3 previousMousePosition;
    //Rigidbody used to handle collisions with the landscape
    private Rigidbody rgb;

    // Initialises the mouse position and retrieves the rigidbody component
    void Start () {
        previousMousePosition = Input.mousePosition;
        this.rgb = this.gameObject.GetComponent<Rigidbody>();
	}
	
	// Updates the camera based on used input
	void Update () {
        Vector3 newMousePosition = Input.mousePosition;
        Vector3 mouseDelta = previousMousePosition - newMousePosition;

        //Adjust based on mouse input
        if(Mathf.Abs(mouseDelta.x) > 1)
        {
            this.transform.Rotate(Vector3.down, horizontalRotationSpeed * mouseDelta.x);
        }if(Mathf.Abs(mouseDelta.y) > 1)
        {
            this.transform.Rotate(Vector3.right, verticalRotationSpeed * mouseDelta.y);
        }
        previousMousePosition = newMousePosition;


        //Adjust based on keyboard input
        float roll = Input.GetAxis("Horizontal");
        if (!roll.Equals(0))
        {
            this.transform.Rotate(Vector3.forward, rollSpeed * roll);
        }float move = Input.GetAxis("Vertical");
        if (!move.Equals(0))
        {
            rgb.AddRelativeForce(Vector3.forward * moveSpeed * move);
        }else
        {
            rgb.velocity = Vector3.zero;
        }
        


    }
}
