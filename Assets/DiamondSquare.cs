﻿using UnityEngine;
using System.Collections;


/**Class to generate a height map using the diamond square
 * algorithm, returns the height map as a 2D float array
 * 
 * Based on description of diamond square from wikipedia
 * **Modified by Stewart Collins - 326206 - Last Edit 11/09/16
 */
public static class DiamondSquare {

    //Generates a height map based on the stated map size
    public static float[,] GenerateHeightMap(int mapSize)
    {
        //The ranges for the starting corner points
        float MAXINITIAL = 1;
        float MININITIAL = 0;

        //The heightmap to be returned
        float[,] heightmap = new float[mapSize, mapSize];

        //Initialise all values to -1
        for (int i = 0; i < mapSize; i++)
        {
            for (int j = 0; j < mapSize; j++)
            {
                heightmap[i, j] = -1;
            }
        }

        //First set the four corner values
        heightmap[0, 0] = Random.Range(MININITIAL, MAXINITIAL);
        heightmap[mapSize - 1, 0] = Random.Range(MININITIAL, MAXINITIAL);
        heightmap[0, mapSize - 1] = Random.Range(MININITIAL, MAXINITIAL);
        heightmap[mapSize - 1, mapSize - 1] = Random.Range(MININITIAL, MAXINITIAL);

        int stepSize = mapSize;
        float changeSize = MAXINITIAL;

        //First calculate all the square based on step size, then calculate the resultant diamonds.
        while (stepSize > 1)
        {
            int offset = stepSize / 2;
            for (int xPos = offset; xPos < (mapSize - 1); xPos += stepSize)
            {
                for (int yPos = offset; yPos < (mapSize - 1); yPos += stepSize)
                {
                    setSquare(heightmap, xPos, yPos, stepSize, mapSize, changeSize);

                }
            }
            for (int xPos = offset; xPos < (mapSize - 1); xPos += stepSize)
            {
                for (int yPos = offset; yPos < (mapSize - 1); yPos += stepSize)
                {
                    //Call on the generated diamonds
                    //Top diamond
                    setDiamond(heightmap, xPos, yPos - (offset), stepSize, mapSize, changeSize);
                    //Bottom diamond
                    setDiamond(heightmap, xPos, yPos + (offset), stepSize, mapSize, changeSize);
                    //Left diamond
                    setDiamond(heightmap, xPos - (offset), yPos, stepSize, mapSize, changeSize);
                    //Right diamond
                    setDiamond(heightmap, xPos + (offset), yPos, stepSize, mapSize, changeSize);
                }
            }
            stepSize /= 2;
            changeSize *= 0.7f;
        }

        return heightmap;

    }

    /**Returns the values of given positions on the height map
     * Takes a string input describing the position relative to the point being calculated
     * Squares take TopLeft, TopRight, BottomLeft, BottomRight
     * Diamonds take Top, Left, Right, Bottom
     */
    private static float getValueAtPosition(string position, float[,] heightmap, int centreX, int centreY, int squareSize)
    {
        float value = -1;
        int xPos = -1;
        int yPos = -1;

        switch (position)
        {
            case "TopLeft":
                xPos = centreX - (squareSize / 2);
                yPos = centreY - (squareSize / 2);
                value = heightmap[yPos, xPos];
                break;

            case "TopRight":
                xPos = centreX + (squareSize / 2);
                yPos = centreY - (squareSize / 2);
                value = heightmap[yPos, xPos];
                break;

            case "BottomLeft":
                xPos = centreX - (squareSize / 2);
                yPos = centreY + (squareSize / 2);
                value = heightmap[yPos, xPos];
                break;

            case "BottomRight":
                xPos = centreX + (squareSize / 2);
                yPos = centreY + (squareSize / 2);
                value = heightmap[yPos, xPos];
                break;

            case "Top":
                xPos = centreX;
                yPos = centreY - (squareSize / 2);
                value = heightmap[yPos, xPos];
                break;

            case "Bottom":
                xPos = centreX;
                yPos = centreY + (squareSize / 2);
                value = heightmap[yPos, xPos];
                break;

            case "Left":
                xPos = centreX - (squareSize / 2);
                yPos = centreY;
                value = heightmap[yPos, xPos];
                break;

            case "Right":
                xPos = centreX + (squareSize / 2);
                yPos = centreY;
                value = heightmap[yPos, xPos];
                break;

            default:
                Debug.Log("Error reading position in diamond square algorithm, set value function");
                break;
        }
        if (value < 0)
        {
            Debug.Log("Error reading position in diamond square algorithm, set value function, xPos:" + xPos + "   yPos:" + yPos + "  Position:" + position + "   Size:" + squareSize + "  CentreX:" + centreX + "  CentreY:" + centreY);
        }
        return value;
    }

    /**Calculates the value of the centre of a given square with its centre at centreX and centreY
     * Heightmap is the heightmap, squareSize is the size of the square, and randomRange is the variation
     * in the random component to be added
     */
    private static void setSquare(float[,] heightmap, int centreX, int centreY, int squareSize, int mapSize, float randomRange)
    {

        if (centreX < 0 || centreX >= mapSize || centreY < 0 || centreY >= mapSize)
        {
            return;
        }

        //If height has already been set then return
        if (heightmap[centreY, centreX] > 0)
        {
            return;
        }

        //The corner values of the square
        float corner1 = getValueAtPosition("TopLeft", heightmap, centreX, centreY, squareSize);
        float corner2 = getValueAtPosition("TopRight", heightmap, centreX, centreY, squareSize);
        float corner3 = getValueAtPosition("BottomLeft", heightmap, centreX, centreY, squareSize);
        float corner4 = getValueAtPosition("BottomRight", heightmap, centreX, centreY, squareSize);

        //The random value to be added to the point
        float random = Random.Range(-randomRange, randomRange);

        //The value the position will be set to
        float value = (corner1 + corner2 + corner3 + corner4 + random) / 4;
        if (value < 0)
        {
            value = 0;
        }
        if (value > 1)
        {
            value = 1;
        }
        heightmap[centreY, centreX] = value;
    }

    /**Calculates the value of the centre of a given diamond with its centre at centreX and centreY
    * Heightmap is the heightmap, diamondSize is the size of the diamond, and randomRange is the variation
    * in the random component to be added
    */
    private static void setDiamond(float[,] heightmap, int centreX, int centreY, int diamondSize, int mapSize, float randomRange)
    {
        if (centreX < 0 || centreX >= mapSize || centreY < 0 || centreY >= mapSize)
        {
            return;
        }
        //If height has already been set then return
        if (heightmap[centreY, centreX] > 0)
        {
            return;
        }

        //Used to determine if the point is on the edge of the map
        bool onEdge = false;
        //The position of the map edge
        int mapEdge = mapSize - 1;

        //The corner values of the square
        float corner1 = -1;
        float corner2 = -1;
        float corner3 = -1;
        float corner4 = -1;

        //The random value to be added to the point
        float random = Random.Range(-randomRange, randomRange);

        //The value the position will be set to
        float value = -1;

        //If it is on an edge then only 3 positions are needed to calculate the value
        if (centreX == 0 || centreX == mapEdge || centreY == 0 || centreY == mapEdge)
        {
            onEdge = true;
            if (centreX == 0)
            {
                corner1 = getValueAtPosition("Top", heightmap, centreX, centreY, diamondSize);
                corner2 = getValueAtPosition("Right", heightmap, centreX, centreY, diamondSize);
                corner3 = getValueAtPosition("Bottom", heightmap, centreX, centreY, diamondSize);
            }
            else if (centreX == mapEdge)
            {
                corner1 = getValueAtPosition("Top", heightmap, centreX, centreY, diamondSize);
                corner2 = getValueAtPosition("Left", heightmap, centreX, centreY, diamondSize);
                corner3 = getValueAtPosition("Bottom", heightmap, centreX, centreY, diamondSize);
            }
            else if (centreY == 0)
            {
                corner1 = getValueAtPosition("Bottom", heightmap, centreX, centreY, diamondSize);
                corner2 = getValueAtPosition("Left", heightmap, centreX, centreY, diamondSize);
                corner3 = getValueAtPosition("Right", heightmap, centreX, centreY, diamondSize);
            }
            else if (centreY == mapEdge)
            {
                corner1 = getValueAtPosition("Top", heightmap, centreX, centreY, diamondSize);
                corner2 = getValueAtPosition("Left", heightmap, centreX, centreY, diamondSize);
                corner3 = getValueAtPosition("Right", heightmap, centreX, centreY, diamondSize);
            }
        }
        //Otherwise get all four positions
        else
        {
            corner1 = getValueAtPosition("Top", heightmap, centreX, centreY, diamondSize);
            corner2 = getValueAtPosition("Right", heightmap, centreX, centreY, diamondSize);
            corner3 = getValueAtPosition("Left", heightmap, centreX, centreY, diamondSize);
            corner4 = getValueAtPosition("Bottom", heightmap, centreX, centreY, diamondSize);
        }

        if (!onEdge)
        {
            value = (corner1 + corner2 + corner3 + corner4 + random) / 4;
        }
        else
        {
            value = (corner1 + corner2 + corner3 + random) / 3;
        }//If the random value makes it higher or lower than max or min then set to max or min
        if (value < 0)
        {
            value = 0;
        }
        if (value > 1)
        {
            value = 1;
        }
        heightmap[centreY, centreX] = value;

    }
}
