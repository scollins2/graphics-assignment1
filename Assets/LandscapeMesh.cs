﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**This class calls diamondsquare to get a heightmap,
 * then creates a mesh from the heightmap and calculates the normals
 * 
 * Parts of this code adapted from CubeScript provided in workshops
 * Modified by Stewart Collins - 326206 - Last Edit 11/09/16
 */

public class LandscapeMesh : MonoBehaviour
{
    //Set the map qualities
    public int sideWidth = 200;
    public int sideLength = 200;
    public int sideHeight = 3000;
    public int mapSize = 65;

    //The colors for the verticies
    public Color sandColor;
    public Color grassColor;
    public Color mountainColor;
    public Color snowColor;

    //The cut off point for the vertex colors
    public float sandEnd = 0.2f;
    public float grassEnd = 0.5f;
    public float mountainEnd = 0.8f;

    //The shader for the polygon
    public Shader shader;

    //The light source
    public Sun sun;

    //The water plane
    public GameObject seaPlane;



    // Called during intialisation
    void Start()
    {
        //Add a renderer and set the shader
        MeshRenderer renderer = this.gameObject.AddComponent<MeshRenderer>();
        renderer.material.shader = this.shader;
        //Create the landscape mesh
        MeshFilter landscapeMesh = this.gameObject.AddComponent<MeshFilter>();
        landscapeMesh.mesh = this.CreateLandscapeMesh(DiamondSquare.GenerateHeightMap(mapSize), mapSize);
        //Add a colider for the mesh and set to convex so collisions will be registered
        MeshCollider collider = this.gameObject.AddComponent<MeshCollider>();
        collider.sharedMesh = landscapeMesh.mesh;
        collider.convex = true;
        //Instantiate the water plane
        GameObject sea = Instantiate(seaPlane);
        sea.transform.parent = this.gameObject.transform;

    }

    // Updates the values used by the shader
    void Update()
    {
        // Get renderer component (in order to pass params to shader)
        MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();

        // Pass updated light positions to shader
        renderer.material.SetColor("_SunColor", this.sun.color);
        renderer.material.SetVector("_SunPosition", this.sun.GetWorldPosition());
    }

    

    //Method to create a landscape mesh with verticies colored based on height
    Mesh CreateLandscapeMesh(float[,] heightMap, int length)
    {
        Mesh m = new Mesh();
        m.name = "Landscape";

        List<Vector3> verticiesList = new List<Vector3>();
        List<Color> colorList = new List<Color>();

        //Add all the veticies for each triangle to the vertex array and set their colors
        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < length; j++)
            {
                if (((i + 1) < length) && ((j + 1) < length))
                {
                    verticiesList.Add(new Vector3((i * sideWidth), heightMap[i, j] * sideHeight, j * sideLength));
                    colorList.Add(getVertexColor(heightMap[i, j]));
                    verticiesList.Add(new Vector3((i + 1) * sideWidth, heightMap[i + 1, j + 1] * sideHeight, (j + 1) * sideLength));
                    colorList.Add(getVertexColor(heightMap[i + 1, j + 1]));
                    verticiesList.Add(new Vector3((i + 1) * sideWidth, heightMap[i + 1, j] * sideHeight, j * sideLength));
                    colorList.Add(getVertexColor(heightMap[i + 1, j]));
                    verticiesList.Add(new Vector3(i * sideWidth, heightMap[i, j] * sideHeight, j * sideLength));
                    colorList.Add(getVertexColor(heightMap[i, j]));
                    verticiesList.Add(new Vector3(i * sideWidth, heightMap[i, j + 1] * sideHeight, (j + 1) * sideLength));
                    colorList.Add(getVertexColor(heightMap[i, j + 1]));
                    verticiesList.Add(new Vector3((i + 1) * sideWidth, heightMap[i + 1, j + 1] * sideHeight, (j + 1) * sideLength));
                    colorList.Add(getVertexColor(heightMap[i + 1, j + 1]));

                }
            }
        }
        m.vertices = verticiesList.ToArray();
        m.colors = colorList.ToArray();

        // Due to inefficient repetition of verticies in vertex array triangles is trivial based on the
        // order that verticies were added to the vertex array
        int[] triangles = new int[m.vertices.Length];
        for (int k = 0; k < m.vertices.Length; k++)
            triangles[k] = k;

        m.triangles = triangles;


        // Calculate surface normals for each face
        List<Vector3> surfaceNormals = new List<Vector3>();
        Vector3 p1, p2, p3, p4, p5, p6;
        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < length; j++)
            {
                if (((i + 1) < length) && ((j + 1) < length))
                {
                    //Done in pairs of two triangles as input to the vertex array
                    p1 = new Vector3((i * sideWidth), heightMap[i, j] * sideHeight, j * sideLength);
                    p2 = new Vector3((i + 1) * sideWidth, heightMap[i + 1, j + 1] * sideHeight, (j + 1) * sideLength);
                    p3 = new Vector3((i + 1) * sideWidth, heightMap[i + 1, j] * sideHeight, j * sideLength);
                    p4 = new Vector3(i * sideWidth, heightMap[i, j] * sideHeight, j * sideLength);
                    p5 = new Vector3(i * sideWidth, heightMap[i, j + 1] * sideHeight, (j + 1) * sideLength);
                    p6 = new Vector3((i + 1) * sideWidth, heightMap[i + 1, j + 1] * sideHeight, (j + 1) * sideLength);

                    //Calculate the first triangle surface normal
                    Vector3 normal1 = new Vector3();
                    Vector3 normalU = p2 - p1;
                    Vector3 normalV = p3 - p1;
                    normal1.x = (normalU.y * normalV.z) - (normalU.z * normalV.y);
                    normal1.y = (normalU.z * normalV.x) - (normalU.x * normalV.z);
                    normal1.x = (normalU.x * normalV.y) - (normalU.y * normalV.x);

                    //Calculate the second triangle surface normal
                    Vector3 normal2 = new Vector3();
                    normalU = p5 - p4;
                    normalV = p6 - p4;
                    normal2.x = (normalU.y * normalV.z) - (normalU.z * normalV.y);
                    normal2.y = (normalU.z * normalV.x) - (normalU.x * normalV.z);
                    normal2.x = (normalU.x * normalV.y) - (normalU.y * normalV.x);

                    //Add surface normals to list
                    surfaceNormals.Add(normal1);
                    surfaceNormals.Add(normal2);
                }
            }
        }

        //Calculate vertex normals for each point based on surface normals
        List<Vector3> vertexNormals = new List<Vector3>();
        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < length; j++)
            {
                //Get connected surfaces normals from surface normal array
                List<Vector3> adjacentFaces = new List<Vector3>();
                int faceIndex;
                //If it not on the starting edge then get faces behind
                if (j > 0)
                {
                    //If it not on the starting edge then get faces behind
                    if (i > 0)
                    {
                        faceIndex = (i - 1) * 2 + ((j - 1) * (length - 1) * 2);
                        adjacentFaces.Add(surfaceNormals[faceIndex]);
                        faceIndex = (i - 1) * 2 + ((j - 1) * (length - 1) * 2) + 1;
                        adjacentFaces.Add(surfaceNormals[faceIndex]);
                    }//If it not on the finishing edge then get faces in front
                    if (i < (length - 1))
                    {
                        faceIndex = (i * 2) + ((j - 1) * (length - 1) * 2);
                        adjacentFaces.Add(surfaceNormals[faceIndex]);
                    }

                }//If it not on the finishing edge then get faces in front
                if (j < (length - 1))
                {
                    if (i > 0)
                    {
                        faceIndex = (i * 2) + (j * (length - 1) * 2) - 1;
                        adjacentFaces.Add(surfaceNormals[faceIndex]);
                    }
                    if (i < (length - 1))
                    {
                        faceIndex = (i * 2) + (j * (length - 1) * 2);
                        adjacentFaces.Add(surfaceNormals[faceIndex]);
                        faceIndex = (i * 2) + (j * (length - 1) * 2) + 1;
                        adjacentFaces.Add(surfaceNormals[faceIndex]);
                    }
                }//Calculate the vertex normal
                Vector3 vertexNormal = new Vector3();
                vertexNormal = Vector3.zero;
                for (int k = 0; k < adjacentFaces.Count; k++)
                {
                    vertexNormal += adjacentFaces[k];
                }
                vertexNormals.Add(vertexNormal);

            }
        }//Due to repetition of verticies in vertex array, assign vertex normals
        //to correct position in array
        List<Vector3> polygonVertexNormals = new List<Vector3>();
        for (int i = 0; i < length - 1; i++)
        {
            for (int j = 0; j < length - 1; j++)
            {
                if (j == 0)
                {
                    polygonVertexNormals.Add(surfaceNormals[i]);
                    polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length)) -1 + i + 1]);
                    polygonVertexNormals.Add(surfaceNormals[i + 1]);
                    polygonVertexNormals.Add(surfaceNormals[i]);
                    polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length)) -1 + i]);
                    polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length)) -1 + i + 1]);
                }
                else
                {
                    polygonVertexNormals.Add(surfaceNormals[(j * (length)) -1 + i]);
                    polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length )) - 1 + i + 1]);
                    polygonVertexNormals.Add(surfaceNormals[(j * (length)) -1 + i + 1]);
                    polygonVertexNormals.Add(surfaceNormals[(j * (length)) -1  + i]);
                    polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length)) - 1 + i]);
                    polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length)) - 1 + i + 1]);
                }
                
            }
        }
        //Set mesh normals
        m.normals = polygonVertexNormals.ToArray();

        return m;

    }

    //Returns the color the vertex should be based on its height
    public Color getVertexColor(float heightMapValue)
    {
        if (heightMapValue < sandEnd)
        {
            return sandColor;
        }
        else if (heightMapValue < grassEnd)
        {
            return grassColor;
        }
        else if (heightMapValue < mountainEnd)
        {
            return mountainColor;
        }
        else
        {
            return snowColor;
        }
    }
}

/* Spent a while tring to get it working more efficiently by only specifying verticies once and then
 * using triangles to set the polygons. Is much faster and allows for a bigger map but still had some
 * unwanted artifacts on boundry edges didn't manage to get it working properly in time
// Method to create a cube mesh with coloured vertices
Mesh CreateLandscapeMesh(float[,] heightMap, int length)
{
    Mesh m = new Mesh();
    m.name = "Landscape";

    List<Vector3> verticiesList = new List<Vector3>();
    List<Color> colorList = new List<Color>();

    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            if (((i + 1) < length) && ((j + 1) < length))
            {
                verticiesList.Add(new Vector3((i * sideWidth), heightMap[i, j] * sideHeight, j * sideLength));
                colorList.Add(getVertexColor(heightMap[i, j]));
                verticiesList.Add(new Vector3((i + 1) * sideWidth, heightMap[i + 1, j + 1] * sideHeight, (j + 1) * sideLength));
                colorList.Add(getVertexColor(heightMap[i + 1, j + 1]));
                verticiesList.Add(new Vector3((i + 1) * sideWidth, heightMap[i + 1, j] * sideHeight, j * sideLength));
                colorList.Add(getVertexColor(heightMap[i + 1, j]));
                verticiesList.Add(new Vector3(i * sideWidth, heightMap[i, j] * sideHeight, j * sideLength));
                colorList.Add(getVertexColor(heightMap[i, j]));
                verticiesList.Add(new Vector3(i * sideWidth, heightMap[i, j + 1] * sideHeight, (j + 1) * sideLength));
                colorList.Add(getVertexColor(heightMap[i, j + 1]));
                verticiesList.Add(new Vector3((i + 1) * sideWidth, heightMap[i + 1, j + 1] * sideHeight, (j + 1) * sideLength));
                colorList.Add(getVertexColor(heightMap[i + 1, j + 1]));

            }
        }
    }
    m.vertices = verticiesList.ToArray();
    m.colors = colorList.ToArray();

    // Automatically define the triangles based on the number of vertices
    int[] triangles = new int[m.vertices.Length];
    for (int k = 0; k < m.vertices.Length; k++)
        triangles[k] = k;

    m.triangles = triangles;


    // Calculate normals
    List<Vector3> surfaceNormals = new List<Vector3>();
    List<Vector3> vertexNormals = new List<Vector3>();
    List<Vector3> normals = new List<Vector3>();
    Vector3 p1, p2, p3, p4, p5, p6;
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            if (((i + 1) < length) && ((j + 1) < length))
            {
                p1 = new Vector3((i * sideWidth), heightMap[i, j] * sideHeight, j * sideLength);
                p2 = new Vector3((i + 1) * sideWidth, heightMap[i + 1, j + 1] * sideHeight, (j + 1) * sideLength);
                p3 = new Vector3((i + 1) * sideWidth, heightMap[i + 1, j] * sideHeight, j * sideLength);
                p4 = new Vector3(i * sideWidth, heightMap[i, j] * sideHeight, j * sideLength);
                p5 = new Vector3(i * sideWidth, heightMap[i, j + 1] * sideHeight, (j + 1) * sideLength);
                p6 = new Vector3((i + 1) * sideWidth, heightMap[i + 1, j + 1] * sideHeight, (j + 1) * sideLength);

                Vector3 normal1 = new Vector3();
                Vector3 normalU = p2 - p1;
                Vector3 normalV = p3 - p1;
                normal1.x = (normalU.y * normalV.z) - (normalU.z * normalV.y);
                normal1.y = (normalU.z * normalV.x) - (normalU.x * normalV.z);
                normal1.x = (normalU.x * normalV.y) - (normalU.y * normalV.x);

                Vector3 normal2 = new Vector3();
                normalU = p5 - p4;
                normalV = p6 - p4;
                normal2.x = (normalU.y * normalV.z) - (normalU.z * normalV.y);
                normal2.y = (normalU.z * normalV.x) - (normalU.x * normalV.z);
                normal2.x = (normalU.x * normalV.y) - (normalU.y * normalV.x);

                surfaceNormals.Add(normal1);
                surfaceNormals.Add(normal2);
                normals.Add(normal1);
                normals.Add(normal1);
                normals.Add(normal1);
                normals.Add(normal2);
                normals.Add(normal2);
                normals.Add(normal2);
            }
        }

    }
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            List<Vector3> adjacentFaces = new List<Vector3>();
            int faceIndex;
            if (j > 0)
            {
                if (i > 0)
                {
                    faceIndex = (i - 1) * 2 + ((j - 1) * (length - 1) * 2);
                    adjacentFaces.Add(surfaceNormals[faceIndex]);
                    faceIndex = (i - 1) * 2 + ((j - 1) * (length - 1) * 2) + 1;
                    adjacentFaces.Add(surfaceNormals[faceIndex]);
                }
                if (i < (length - 1))
                {
                    faceIndex = (i * 2) + ((j - 1) * (length - 1) * 2);
                    adjacentFaces.Add(surfaceNormals[faceIndex]);
                }

            }
            if (j < (length - 1))
            {
                if (i > 0)
                {
                    faceIndex = (i * 2) + (j * (length - 1) * 2) - 1;
                    adjacentFaces.Add(surfaceNormals[faceIndex]);
                }
                if (i < (length - 1))
                {
                    faceIndex = (i * 2) + (j * (length - 1) * 2);
                    adjacentFaces.Add(surfaceNormals[faceIndex]);
                    faceIndex = (i * 2) + (j * (length - 1) * 2) + 1;
                    adjacentFaces.Add(surfaceNormals[faceIndex]);
                }
            }
            Vector3 vertexNormal = new Vector3();
            vertexNormal = Vector3.zero;
            for (int k = 0; k < adjacentFaces.Count; k++)
            {
                vertexNormal += adjacentFaces[k];
            }
            vertexNormals.Add(vertexNormal);

        }
    }
    List<Vector3> polygonVertexNormals = new List<Vector3>();
    for (int i = 0; i < length - 1; i++)
    {
        for (int j = 0; j < length - 1; j++)
        {
            polygonVertexNormals.Add(surfaceNormals[(j * (length - 1)) + i]);
            polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length - 1)) + i + 1]);
            polygonVertexNormals.Add(surfaceNormals[(j * (length - 1)) + i + 1]);
            polygonVertexNormals.Add(surfaceNormals[(j * (length - 1)) + i]);
            polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length - 1)) + i]);
            polygonVertexNormals.Add(surfaceNormals[((j + 1) * (length - 1)) + i + 1]);
        }
    }

    m.normals = polygonVertexNormals.ToArray();
    //m.normals = normals.ToArray();


    return m;

}*/