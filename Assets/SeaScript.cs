﻿using UnityEngine;
using System.Collections;

/**This class handles the plane that is meant to represent
 * water in the landscape, setting the shader and passing some information
 * to the shader
 * Modified by Stewart Collins - 326206 - Last Edit 11/09/16
 */
public class SeaScript : MonoBehaviour {
    //The shader for the water
    public Shader seaShader;
    //The color of the water
    public Color seaColor;
    //How opaque the water is
    public float waterOpacity = 0.5f;
    //The light source the shader uses
    private Sun sun;
    
    //Set initial values
    void Start () {
        //Set the renderer for the sea
        MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();
        renderer.material.shader = seaShader;
        //Find the sun light source
        sun = FindObjectOfType<Sun>();
    }
   
    //Updates shader information
    void Update()
    {
        MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();

        // Pass the sea color to the shader
        renderer.material.SetColor("_SeaColor", seaColor);

        // Pass updated light positions to shader
        renderer.material.SetColor("_SunColor", this.sun.color);
        renderer.material.SetVector("_SunPosition", this.sun.GetWorldPosition());
        renderer.material.SetFloat("_Opacity", waterOpacity);
    }
	
}
