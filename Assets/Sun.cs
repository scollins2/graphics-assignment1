﻿using UnityEngine;
using System.Collections;

/**Simple class for a sphere that revolves around the landscape
 * and acts as a light source.
 * **Modified by Stewart Collins - 326206 - Last Edit 11/09/16
 */
public class Sun : MonoBehaviour {
    //The color of the light eminating from the source
    public Color color;
    //The speed of the rotation
    public float rotationAngle = 5;
    //The position the sun rotates around
    public Transform sunPivot;

    //Returns the position of the sun
    public Vector3 GetWorldPosition()
    {
        return this.transform.position;
    }

    //Rotates the sun around the pivot
    public void Update()
    {
        this.transform.RotateAround(sunPivot.position, Vector3.right,  rotationAngle * Time.deltaTime);
    }
}